# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATHech
# Path to your oh-my-zsh installation.
export ZSH="/home/user/.oh-my-zsh"
export TERM="xterm-256color"
export PATH="$HOME/Projects/bash:$PATH"
export FZF_BASE="$HOME/.fzf"
export FZF_DEFAULT_COMMAND='sudo ag --hidden --ignore-dir={var,run,sys,proc,.git} -f -g ""'
export PATH="$HOME/.gem/ruby/2.6.0/bin:$PATH"
export EDITOR="nvim"
export ANDROID_SDK="$HOME/Android/Sdk"

ZSH_THEME="powerlevel9k/powerlevel9k"
POWERLEVEL9K_MODE='nerdfont-complete'
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_beginning"
POWERLEVEL9K_RVM_BACKGROUND="black"
POWERLEVEL9K_RVM_FOREGROUND="249"
POWERLEVEL9K_RVM_VISUAL_IDENTIFIER_COLOR="red"
POWERLEVEL9K_TIME_BACKGROUND="black"
POWERLEVEL9K_TIME_FOREGROUND="249"
POWERLEVEL9K_TIME_FORMAT="%D{%I:%M}"
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='black'
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='green'
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='black'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='yellow'
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='white'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='black'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='black'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='blue'
POWERLEVEL9K_FOLDER_ICON=''
POWERLEVEL9K_STATUS_OK_IN_NON_VERBOSE=true
POWERLEVEL9K_STATUS_VERBOSE=false
POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=0
POWERLEVEL9K_VCS_UNTRACKED_ICON='\u25CF'
POWERLEVEL9K_VCS_UNSTAGED_ICON='\u00b1'
POWERLEVEL9K_VCS_INCOMING_CHANGES_ICON='\u2193'
POWERLEVEL9K_VCS_OUTGOING_CHANGES_ICON='\u2191'
POWERLEVEL9K_VCS_COMMIT_ICON="\uf417"
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="%F{blue}\u256D\u2500%f"
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%F{blue}\u2570\uf460%f "
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context ssh dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(command_execution_time  status  time)
HIST_STAMPS="mm/dd/yyyy"
DISABLE_UPDATE_PROMPT=true

plugins=(
  git
  fzf
  zsh-syntax-highlighting
  zsh-autosuggestions
  extract
  sudo
  npm
  pip
  python
  vi-mode
)
ZSH_DISABLE_COMPFIX=true

source $ZSH/oh-my-zsh.sh

alias ll='colorls -lA --sd --group-directories-first'
alias ls='colorls -A --group-directories-first'
alias vim="nvim"
alias vi="nvim"
alias sudo="sudo "
alias mux="tmuxinator"
alias :q="exit"
alias :e="nvim"
alias update="python /home/user/Projects/python/scripts/update.py"
alias updatemirrors="python /home/user/Projects/python/scripts/updatemirrors.py"
alias grubmkconfig="sudo grub-mkconfig -o /boot/grub/grub.cfg"
alias myre="ls /sys/class/net/mullvad*"
alias backupkeys="sudo cp /run/media/user/Container/keepassxc/keys.kdbx ~/keepassxc; sudo cp /run/media/user/Container/keepassxc/keys.kdbx /run/media/user/USB\ Storage/keepassxc; echo 'Keys copied to USB Storage, HDD Storage and ~/keepassxc'"
alias bbclean="python /home/user/Projects/python/scripts/bleachbit.py"
alias please='sudo $(fc -ln -1)'
alias .='cd ..'
alias ..='cd ../../'
alias ...='cd ../../../'
alias neofetch='neofetch --off --color_blocks off --gtk2 off --gtk3 off --underline off --disable title --disable wm --disable shell --disable resolution --disable distro'
alias rm='rm -I'
alias flaskmwd='tmuxinator start flaskmwd'
alias flaskaes='tmuxinator start flaskaes'
alias djangodev='tmuxinator start djangodev'
alias vuedev='tmuxinator start vuedev'
alias reactnative='tmuxinator start reactnative'
alias ping='ping google.com -c 3'
alias gs='git status'
alias ga='git add -A'
alias gc='git commit -m'
alias gco='git checkout'
alias gm='git merge'
alias gppm='git push production master'
alias venv='source venv/bin/activate'
alias pfreeze='pip freeze > requirements.txt'
alias migrate='python3 manage.py makemigrations && python3 manage.py migrate'

bindkey '^F' fzf-file-widget
wal -q -R

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# set default python env at startup
conda activate py38


# remove duplicates in $PATH
typeset -aU path


# vim mode config
# ---------------

# Activate vim mode.
bindkey -v

# Remove mode switching delay.
KEYTIMEOUT=5

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[2 q'

  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[6 q'
  fi
}
zle -N zle-keymap-select

# Use beam shape cursor on startup.
echo -ne '\e[6 q'

# Use beam shape cursor for each new prompt.
preexec() {
   echo -ne '\e[6 q'
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Tab completion
autoload -Uz compinit && compinit
zstyle ':completion:*' menu yes select
