call plug#begin('~/.config/nvim/bundle')
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'sheerun/vim-polyglot'
Plug 'zchee/deoplete-jedi'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-gitgutter'
Plug 'itchyny/lightline.vim'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'w0rp/ale'
Plug 'Yggdroot/indentLine'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'ntpeters/vim-better-whitespace'
Plug 'tpope/vim-fugitive'
Plug 'lervag/vimtex'
Plug 'mhinz/neovim-remote'
" call PlugInstall to install new plugins
call plug#end()


" basics
filetype plugin indent on
syntax on
set number
set relativenumber
set incsearch
set ignorecase
set smartcase
set nohlsearch
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab
set nobackup
set noswapfile
set wrap
set modifiable
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite

"NERDTree
" How can I close vim if the only window left open is a NERDTree?
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" toggle NERDTree
map <C-n> :NERDTreeToggle<CR>
" open NERDTree at given dir
com! -nargs=1 -complete=dir Ncd NERDTree | cd <args> |NERDTreeCWD
" open NERDTree on startup
autocmd VimEnter * NERDTree
" general NERDTree settings
set autochdir
let g:NERDTreeChDirMode=2
let g:NERDTreeIgnore=['\.rbc$', '\~$', '\.pyc$', '\.db$', '\.sqlite$', '__pycache__', 'node_modules']
let g:NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$', '\.bak$', '\~$']
let g:NERDTreeShowBookmarks=1
let g:nerdtree_tabs_focus_on_files=1
let g:NERDTreeMapOpenInTabSilent = '<RightMouse>'
let g:NERDTreeShowHidden=1
let g:NERDTreeMinimalUI=1
let g:NERDTreeQuitOnOpen=1
let g:NERDTreeHighlightCursorline = 0
" devicons in NERDTree
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1
let NERDTreeDirArrowExpandable = "\u00a0"
let NERDTreeDirArrowCollapsible = "\u00a0"

" deoplete
let g:deoplete#enable_at_startup = 1
" use tab to forward cycle
inoremap <silent><expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
" use tab to backward cycle
inoremap <silent><expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
" Close the documentation window when completion is done
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" execute python file on save
map <C-s> <Esc>:w<CR>:!python %:p<CR>
" alternative python execution with terminal mode
" map <C-s> <Esc>:w<CR>:below 10split \| terminal python %:p<CR>:startinsert<CR>
" tnoremap <Esc> <C-\><C-n>

" tell nvim which python env to use
let g:python3_host_prog = '/home/mike/.conda/envs/py37/bin/python'

" unbind directional arrow keys
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
noremap   <Up>     <NOP>
noremap   <Down>   <NOP>
noremap   <Left>   <NOP>
noremap   <Right>  <NOP>

" bind new leader from \ to ,
let mapleader = ","

" UltiSnips settings
let g:UltiSnipsExpandTrigger="<F4>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"

" Change tmux split line color
hi VertSplit cterm=NONE guibg=NONE ctermfg=Green
hi CursorLine ctermfg=Green

" Ale
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_linters = {'python': ['flake8']}
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['autopep8', 'isort'],
\}
let g:ale_fix_on_save = 0
let g:ale_pattern_options = {
\   '__init__.py': {'ale_enabled': 0},
\}

" Indent line guides
let g:indentLine_char = '▏'
let g:indentLine_setColors = 1
let g:indentLine_color_term = 2

" fuzzy finder
nmap <C-f> :FZF<CR>

" allow vim to copy to system clipboard requires xclip (yay)
set clipboard=unnamedplus

" whitespace settings
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:strip_whitespace_confirm=0

" Save file with sudo
command! -nargs=0 WriteWithSudo w !sudo tee % > /dev/null
" Use :rwq instead of :WriteWithSudo
cnoreabbrev w!! WriteWithSudo

" lightline config
let g:lightline = {
  \   'active': {
  \     'left':[ [ 'mode', 'paste' ],
  \              [ 'gitbranch', 'readonly', 'filename', 'modified' ]
  \     ]
  \   },
	\   'component': {
	\     'lineinfo': '%3l:%-2v',
	\   },
  \   'component_function': {
  \     'gitbranch': 'fugitive#head',
  \   }
  \ }

let g:lightline.separator = {
	\   'left': '', 'right': ''
  \}
let g:lightline.subseparator = {
	\   'left': '', 'right': ''
  \}

"" allow flask snippets in html and python files
autocmd FileType python set ft=python.django
autocmd FileType html set ft=htmldjango.html

" cycle through tabs
nnoremap <C-PageUp> :tabnext<CR>
nnoremap <C-PageDown>   :tabprevious<CR>
inoremap <C-PageUp> <Esc>:tabnext<CR>i
inoremap <C-PageDown>   <Esc>:tabprevious<CR>i

" move tabs
nnoremap <C-S-PageUp>   :tabmove +1<CR>
nnoremap <C-S-PageDown> :tabmove -1<CR>
inoremap <C-S-PageUp>   :tabmove +1<CR>
inoremap <C-S-PageDown> :tabmove -1<CR>

" open all files in new tabs of CWD in NERDTree
nnoremap <C-t> :args **/*.*<CR>:tab all<CR>
inoremap <C-t> <Esc>:args **/*.*<CR>:tab all<CR>

" fix broken default colors with pywal
:highlight Directory ctermfg=2
:highlight PreProc ctermfg=13
:highlight MoreMsg ctermfg=13
:highlight Type ctermfg=13

"Vimtex
call deoplete#custom#var('omni', 'input_patterns', {
      \ 'tex': g:vimtex#re#deoplete
      \})

let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_view_general_viewer = 'zathura'

:highlight Folded ctermbg=NONE ctermfg=blue guibg=NONE guifg=blue
