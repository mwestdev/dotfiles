import os

# If node is running kill it
is_node_running = os.popen("ps -e | grep node | wc -l").read()
if int(is_node_running) > 0:
    os.system("sudo pkill node")
