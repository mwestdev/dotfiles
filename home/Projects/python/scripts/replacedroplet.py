import requests
import json
import os
import time

api_token = ''
api_url_base = 'https://api.digitalocean.com/v2/'
headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

def get_droplet_info():

    api_url = '{0}droplets'.format(api_url_base)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None

droplet_info = get_droplet_info()

if droplet_info is not None:
    for i in droplet_info['droplets']:
        if i['name'] == 'OpenVPNServer':
            print('Found VPN Server')
            droplet_id = i['id']
            droplet_ip = i['networks']['v4'][0]['ip_address']
else:
    print('[!] Request Failed')

def delete_droplet(droplet_id):

    api_url = '{0}droplets/{1}'.format(api_url_base, droplet_id)

    response = requests.delete(api_url, headers=headers)

    if response.status_code == 204:
        print("Deleted Droplet: " + str(droplet_ip) + " Successfully")
        return
    else:
        return None

# I want to delete the droplet then make a new droplet in its place from the snapshot we have already

def get_snapshot_info():

    api_url = '{0}snapshots'.format(api_url_base)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None

snapshot_info = get_snapshot_info()
if snapshot_info is not None:
    for i in snapshot_info['snapshots']:
        if i['name'] == 'OpenVPNServer':
            snapshot_id = i['id']
else:
    print('[!] Request Failed')

def get_ssh_keys():

    api_url = '{0}account/keys'.format(api_url_base)

    response = requests.get(api_url, headers=headers)

    if response.status_code >= 500:
        print('[!] [{0}] Server Error'.format(response.status_code))
        return None
    elif response.status_code == 404:
        print('[!] [{0}] URL not found: [{1}]'.format(response.status_code,api_url))
        return None
    elif response.status_code == 401:
        print('[!] [{0}] Authentication Failed'.format(response.status_code))
        return None
    elif response.status_code == 400:
        print('[!] [{0}] Bad Request'.format(response.status_code))
        return None
    elif response.status_code >= 300:
        print('[!] [{0}] Unexpected Redirect'.format(response.status_code))
        return None
    elif response.status_code == 200:
        ssh_keys = json.loads(response.content.decode('utf-8'))
        return ssh_keys
    else:
        print('[?] Unexpected Error: [HTTP {0}]: Content: {1}'.format(response.status_code, response.content))
    return None


ssh_keys = get_ssh_keys()

if ssh_keys is not None:
    ssh_keys_array = []
    for i in ssh_keys['ssh_keys']:
        ssh_keys_array.append(i['id'])
else:
    print('[!] Request Failed')


def create_droplet(region):

    api_url = '{0}droplets'.format(api_url_base)

    data = {
        "name": "OpenVPNServer",
        "region": region,
        "size": "s-1vcpu-1gb",
        "image": snapshot_id,
        "ssh_keys": ssh_keys_array
    }

    response = requests.post(api_url, headers=headers, data=json.dumps(data))

    if response.status_code == 202:
        print("Droplet Created Successfully")
        return
    else:
        return None


def choose_region():
    regions = ["lon1", "ams3", "fra1", "nyc1"]
    region_choice = input("Please choose region for the new VPN:\n\nLondon = 1\nAmsterdam = 2\nGermany = 3\nNew York = 4\n\n")
    if float(region_choice).is_integer():
        if 0 < int(region_choice) < 5:
            create_droplet(regions[int(region_choice)-1])
            return
        else:
            print("Please choose a valid option")
            choose_region()
            return
    else:
        print("Please choose a valid option")
        choose_region()
        return


choose_region()
time.sleep(5)

new_droplet_info = get_droplet_info()
if new_droplet_info is not None:
    for i in new_droplet_info['droplets']:
        if i['name'] == 'OpenVPNServer':
            new_droplet_ip = i['networks']['v4'][0]['ip_address']
else:
    print('[!] Request Failed')

delete_droplet(droplet_id)
print("Updating VPN config in 30 seconds on: %s..." % str(new_droplet_ip))
os.system('''sh -c "sleep 30; vpn '%s'"''' % str(new_droplet_ip))
