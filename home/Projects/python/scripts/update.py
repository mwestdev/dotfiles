#!/home/user/.conda/envs/py37/bin/python3
import os

# Update yay packages
os.system("yay -Syyu --noconfirm")

# Remove orphans
orphans = os.popen("yay -Qtdq | wc -l").read()
if int(orphans) > 0:
    print("Removing orphans...")
    os.system("yay -Rns $(pacman -Qtdq) --noconfirm")
else:
    print("No orphans to remove")

# Clean yay cache
os.system("yay -Scc --noconfirm")

# Clear journalctl logs
os.system("sudo journalctl --rotate && sudo journalctl --vacuum-time=1s")

# Update conda and packages
os.system("yes | sudo conda update -n root conda && yes | conda update --all")
