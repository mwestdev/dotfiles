import os

# Bleachbit cleaner
is_firefox_open = os.popen('ps -e | grep firefox | wc -l').read()
if int(is_firefox_open) > 0:
    os.system("pkill firefox && bleachbit -c --preset && i3-msg 'workspace 2; append_layout /home/user/.config/i3/workspace-2.json' && i3-msg 'workspace 1' && sleep 5; firefox-developer-edition </dev/null &>/dev/null &")
else:
    os.system("bleachbit -c --preset")
