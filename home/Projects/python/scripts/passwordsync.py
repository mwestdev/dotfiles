import requests
import json
import time
import pexpect

api_token = ''
api_url_base = 'https://api.digitalocean.com/v2/'
headers = {'Content-Type': 'application/json',
           'Authorization': 'Bearer {0}'.format(api_token)}

def get_droplet_info():

    api_url = '{0}droplets'.format(api_url_base)

    response = requests.get(api_url, headers=headers)

    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None


droplet_info = get_droplet_info()

if droplet_info is not None:
    for i in droplet_info['droplets']:
        if i['name'] == 'OpenVPNServer':
            print('Found VPN Server')
            droplet_id = i['id']
            droplet_ip = i['networks']['v4'][0]['ip_address']
else:
    print('[!] Request Failed')

print(droplet_ip)

def push_to_server():
    child = pexpect.spawn('sftp user@' + droplet_ip)
    child.expect('sftp>')
    child.sendline('put /home/mike/keepassxc/6vZ445f7&PZv.kdbx keepassxc/6vZ445f7&PZv.kdbx')
    child.expect('sftp>')
    child.close()
    print("DB successfully pushed to remote server on: " + droplet_ip)


push_to_server()
