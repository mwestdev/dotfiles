from livereload import Server

server = Server()
server.watch("/home/user/Projects/python/django/timesince.xyz/main/")
server.watch("/home/user/Projects/python/django/timesince.xyz/media/")
server.watch("/home/user/Projects/python/django/timesince.xyz/posts/")
server.watch("/home/user/Projects/python/django/timesince.xyz/timers/")
server.watch("/home/user/Projects/python/django/timesince.xyz/timesince/")
server.watch("/home/user/Projects/python/django/timesince.xyz/users/")
server.serve(liveport=35729)
