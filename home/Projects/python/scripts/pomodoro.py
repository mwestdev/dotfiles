import time
import datetime
import os

def pomodoro():
    try:
        duration = input("How many minutes do you want to do this task for? \n")
        if float(duration).is_integer():
            ready = input("Are you ready? \n")
            if ready == "y" or ready == "yes":
                secs = int(float(duration)) * 60
                os.system("clear")
                while secs > 0:
                    timer = str(datetime.timedelta(seconds=secs))
                    print(timer, end="", flush=True)
                    print("\r", end="", flush=True)
                    time.sleep(1)
                    secs -= 1
                endmessage = "'TIMES UP'"
                os.system(endmessage)
                again = input("You're done, want to go again? \n")
                if again == "y" or again == "yes":
                    pomodoro()
                else:
                    os.system("clear")
            else:
                os.system("clear")
        else:
            print("Please enter an integer")
            pomodoro()
    except ValueError:
        print("Please enter an integer")
        pomodoro()


pomodoro()
