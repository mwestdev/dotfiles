#!/home/user/.conda/envs/py37/bin/python3
from datetime import datetime

now = datetime.now()

with open("/home/user/Projects/python/scripts/crontest.txt", "a") as a_writer:
    a_writer.write("\n" + now.strftime("%d/%m/%Y, %H:%M:%S"))
